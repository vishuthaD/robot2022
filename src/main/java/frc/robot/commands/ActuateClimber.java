// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.Climber.ArmPosition;

public class ActuateClimber extends SequentialCommandGroup {

  public ActuateClimber(Climber climber) {
    super(
      new InstantCommand(() -> climber.setIsClimbing(true)),
      new InstantCommand(() -> climber.setPosition(ArmPosition.IN), climber),
      new InstantCommand(() -> climber.setPosition(ArmPosition.MIDDLE), climber),
      new InstantCommand(() -> climber.setAngle(Value.kForward), climber, climber, climber, climber),
      new InstantCommand(() -> climber.setPosition(ArmPosition.OUT), climber),
      new InstantCommand(() -> climber.setAngle(Value.kReverse), climber, climber, climber, climber),
      new InstantCommand(() -> climber.setPosition(ArmPosition.IN), climber),
      new InstantCommand(() -> climber.setPosition(ArmPosition.MIDDLE), climber),
      new InstantCommand(() -> climber.setAngle(Value.kForward), climber, climber, climber, climber),
      new InstantCommand(() -> climber.setPosition(ArmPosition.OUT), climber),
      new InstantCommand(() -> climber.setAngle(Value.kReverse), climber, climber, climber, climber),
      new InstantCommand(() -> climber.setIsClimbing(false))
    );
  }

}
