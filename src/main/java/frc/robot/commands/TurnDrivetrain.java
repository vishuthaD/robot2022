package frc.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.NavX;

/** A command that will turn the robot to the specified angle. */
public class TurnDrivetrain extends PIDCommand {

  private final static double kP = 0;
  private final static double kI = 0;
  private final static double kD = 0;

  private final static double kPositionTolerance = 1;
  private final static double kVelocityTolerance = 5;

  /**
   * Turns to robot to the specified angle.
   *
   * @param targetAngleDegrees The angle to turn to
   * @param drivetrain The drive subsystem to use
   */
  public TurnDrivetrain(Drivetrain drivetrain, NavX navX, double targetAngleDegrees) {
    super(
        new PIDController(kP, kI, kD),
        // Close loop on heading
        navX::getYaw,
        // Set reference to target
        targetAngleDegrees,
        // Pipe output to turn robot
        output -> drivetrain.arcadeDrive(0, output),
        // Require the drive
        drivetrain);

    // Set the controller to be continuous (because it is an angle controller)
    getController().enableContinuousInput(-180, 180);
    // Set the controller tolerance - the delta tolerance ensures the robot is stationary at the
    // setpoint before it is considered as having reached the reference
    getController()
        .setTolerance(kPositionTolerance, kVelocityTolerance);
  }

  @Override
  public boolean isFinished() {
    // End when the controller is at the reference.
    return getController().atSetpoint();
  }
}
