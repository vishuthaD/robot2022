// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import com.team6479.lib.util.dynamic.Limelight;

import java.util.function.DoubleSupplier;

import com.team6479.lib.util.DistanceCalculator;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Flywheels;

public class SpinUpFlywheels extends CommandBase {

  private final Flywheels flywheels;
  private final Limelight limelight;
  private DistanceCalculator distanceCalculator;
  private DoubleSupplier calculatedDistance;

  public SpinUpFlywheels(Flywheels flywheels, Limelight limelight) {
    this.flywheels = flywheels;
    this.limelight = limelight;
    distanceCalculator = new DistanceCalculator(0, 0, 0); // height of limelight in inches, height of target in inches, angle of limelight in radians
    this.calculatedDistance = () -> distanceCalculator.calculate(Math.toRadians(limelight.getYOffset()));
    addRequirements(this.flywheels);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    SmartDashboard.putNumber("Top FW Speed", flywheels.getTopRPM(calculatedDistance.getAsDouble()));
    SmartDashboard.putNumber("Top FW Error", flywheels.getTopRPM(calculatedDistance.getAsDouble()) - flywheels.getSmallSpeed());
    SmartDashboard.putNumber("Distance from target", calculatedDistance.getAsDouble()); // base of the triangle
    if (limelight.hasTarget()) {
      flywheels.setSpeed(0, flywheels.getTopRPM(calculatedDistance.getAsDouble())); // TODO: make sure the RPM calculation is correct
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return flywheels.isWithinTolerance(calculatedDistance.getAsDouble());
  }
}
