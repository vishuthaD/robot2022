// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import com.team6479.lib.util.dynamic.Limelight;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Turret;

public class AimTurret extends CommandBase {

  private Turret turret;
  private Limelight limelight;

  /** Creates a new AimTurret. */
  public AimTurret(Turret turret, Limelight limelight) {
    this.turret = turret;
    this.limelight = limelight;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(turret); // Limelight is not technically a subsystem (maybe fix this)
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    boolean isCorrected = turret.isCorrected();
    boolean hasTarget = limelight.hasTarget();

    if (!isCorrected && !hasTarget) {
      turret.clearCorrection();
    }

    if (hasTarget && isCorrected) {
      turret.setPosition(turret.getCurrentAngle() + limelight.getXOffset());
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    turret.clearCorrection();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return Math.abs(limelight.getXOffset()) <= 0.5 && limelight.hasTarget();
  }
}
