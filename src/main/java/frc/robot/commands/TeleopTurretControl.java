// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;
import com.team6479.lib.util.dynamic.Limelight;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Turret;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class TeleopTurretControl extends CommandBase {
  private final Turret turret;
  private final Limelight limelight;
  private final DoubleSupplier manualAdjustValue;
  private final BooleanSupplier overrideTrigger;

  private boolean visionDelayState = false;

  public TeleopTurretControl(Turret turret, Limelight limelight, DoubleSupplier manualAdjustValue,
      BooleanSupplier overrideTrigger) {
    this.turret = turret;
    this.limelight = limelight;
    addRequirements(this.turret);

    this.manualAdjustValue = manualAdjustValue;
    this.overrideTrigger = overrideTrigger;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    turret.addClearCorrectionHook(overrideTrigger);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    SmartDashboard.putNumber("Turret Angle", turret.getCurrentAngle());
    boolean isCorrected = turret.isCorrected();
    boolean hasTarget = limelight.hasTarget();
    boolean isOverridden = overrideTrigger.getAsBoolean();

    // We want target searching to begin before correction completes
    if (!isCorrected && !hasTarget && visionDelayState) {
      visionDelayState = false;
    }

    if (hasTarget && (isCorrected || !visionDelayState) && !isOverridden) {
      if (!isCorrected) {
        turret.clearCorrection();
      }
      turret.setPosition(turret.getCurrentAngle() + limelight.getXOffset());
      if (turret.isCorrected()) {
        visionDelayState = true;
      }
    } else if (isCorrected || isOverridden) {
      double joystickValue = manualAdjustValue.getAsDouble();
      turret.setPercentOutput(Math.abs(joystickValue) > 0.1 ? joystickValue : 0);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    turret.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}

