// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.team6479.lib.commands.TeleopTankDrive;
import com.team6479.lib.controllers.CBJoystick;
import com.team6479.lib.controllers.CBXboxController;
import com.team6479.lib.util.dynamic.Limelight;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ConditionalCommand;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.ActuateClimber;
import frc.robot.autos.DriveOutTarmacAuto;
import frc.robot.autos.DriveShootTwoAuto;
import frc.robot.autos.FiveBallAuto;
import frc.robot.autos.LeftSideKnock2BallAuto;
import frc.robot.autos.LeftSideKnock2BallOppositeSideAuto;
import frc.robot.autos.RightSideKnock2BallAuto;
import frc.robot.commands.ColorLogger;
import frc.robot.commands.SpinUpFlywheels;
import frc.robot.commands.TeleopIntakeArm;
import frc.robot.commands.TeleopTurretControl;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheels;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArm;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.IntakeRollers;
import frc.robot.subsystems.NavX;
import frc.robot.subsystems.Turret;
import frc.robot.subsystems.Climber.ArmPosition;
import frc.robot.util.MultiButton;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...

  private final IntakeArm intakeArm = new IntakeArm();
  private final IntakeArmSolenoid intakeArmSolenoid = new IntakeArmSolenoid();

  private CBXboxController xbox =  new CBXboxController(0);
  private final CBJoystick joystick = new CBJoystick(1);

  private Drivetrain drivetrain = new Drivetrain();
  private Turret turret = new Turret(0, 360); // TODO: Change limits
  private Flywheels flywheels = new Flywheels(0, 0); // TODO: Change regression constants
  private Limelight limelight = new Limelight();
  private IntakeRollers intakeRollers = new IntakeRollers();
  private Indexer indexer = new Indexer();
  private Climber climber = new Climber();
  private NavX navX = new NavX();

  private SendableChooser<Command> autonChooser = new SendableChooser<Command>();

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer(){

    // Configure the button bindings
    configureButtonBindings();

    autonChooser.setDefaultOption("Drive shoot two", new DriveShootTwoAuto(drivetrain, navX, intakeArmSolenoid, limelight, turret));
    autonChooser.addOption("Five Ball", new FiveBallAuto(drivetrain, navX, limelight, turret, intakeArmSolenoid, intakeRollers, indexer, flywheels));
    autonChooser.addOption("Left Knock 2", new LeftSideKnock2BallAuto(drivetrain, navX, intakeArmSolenoid, limelight, turret));
    autonChooser.addOption("Left Knock 2 Opposite", new LeftSideKnock2BallOppositeSideAuto(drivetrain, navX, intakeArmSolenoid, intakeRollers, limelight, turret, flywheels, indexer));
    autonChooser.addOption("Right Knock 2", new RightSideKnock2BallAuto(drivetrain, navX, intakeArmSolenoid, limelight, turret, intakeRollers, flywheels, indexer));
    autonChooser.addOption("Drive out of tarmac", new DriveOutTarmacAuto(drivetrain, navX));

    Shuffleboard.getTab("SmartDashboard").add(autonChooser);

    CameraServer.startAutomaticCapture();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    xbox.getButton(XboxController.Button.kA).whenPressed(new InstantCommand(() -> {
      if (intakeRollers.getSpeed() > 0) {
        intakeRollers.rollersOff();
      } else {
        intakeRollers.rollersOn();
      }
    }, intakeRollers));

    intakeArm.setDefaultCommand(new TeleopIntakeArm(intakeArm, () -> xbox.getRightTriggerAxis() > 0, () -> xbox.getLeftTriggerAxis() > 0));

    xbox.getButton(Button.kX)
      .whenPressed(new SequentialCommandGroup(
        new SpinUpFlywheels(flywheels, limelight),
        new InstantCommand(() -> indexer.setTop(1.0), indexer)
      )).whenReleased(new SequentialCommandGroup(
        new InstantCommand(flywheels::off, flywheels),
        new InstantCommand(() -> indexer.setTop(0.0), indexer)
      ));

    xbox.getButton(Button.kRightBumper)
      .whenPressed(new InstantCommand(() -> {
        if(intakeArmSolenoid.getPositon() != Value.kForward) {
          intakeArmSolenoid.push();
        }
      }));

    xbox.getButton(Button.kLeftBumper)
      .whenPressed(new InstantCommand(() -> {
        if(intakeArmSolenoid.getPositon() != Value.kReverse) {
          intakeArmSolenoid.retract();
        }
      }));

    drivetrain.setDefaultCommand(new TeleopTankDrive(drivetrain, () -> xbox.getLeftY(), () -> xbox.getRightX()));

    indexer.setDefaultCommand(new ColorLogger(indexer));

    xbox.getButton(Button.kB).whenPressed(new InstantCommand(() -> {
      indexer.setBottom(-1.0);
      indexer.setTop(-1.0);
    }, indexer)).whenReleased(new InstantCommand(() -> {
      indexer.setBottom(0.0);
      indexer.setTop(0.0);
    }, indexer));
    xbox.getButton(Button.kY)
      .whenPressed(new InstantCommand(() -> indexer.setBottom(1.0), indexer))
      .whenReleased(new InstantCommand(() -> indexer.setBottom(0.0), indexer));

    new MultiButton(xbox, XboxController.Button.kStart.value, XboxController.Button.kBack.value)
      .whenPressed(
        new ConditionalCommand(
          new ConditionalCommand(
            new InstantCommand(),
            new ActuateClimber(climber),
            climber::getIsClimbing),
          new InstantCommand(() -> climber.setPosition(ArmPosition.OUT), climber),
        climber::hasExtended));

  }

  public void teleopInit() {
    turret.setDefaultCommand(new TeleopTurretControl(turret, limelight, joystick::getZ, () -> joystick.getButton(1).get()));
  }

  public void disabledInit() {
    turret.clearCorrection();
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return autonChooser.getSelected();
  }
}
