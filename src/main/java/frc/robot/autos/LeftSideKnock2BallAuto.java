package frc.robot.autos;

import com.team6479.lib.util.dynamic.Limelight;
import com.team6479.lib.util.dynamic.Limelight.CamMode;
import com.team6479.lib.util.dynamic.Limelight.LEDState;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.commands.AimTurret;
import frc.robot.commands.StraightDrive;
import frc.robot.commands.TurnDrivetrain;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.NavX;
import frc.robot.subsystems.Turret;

public class LeftSideKnock2BallAuto extends SequentialCommandGroup {
    public LeftSideKnock2BallAuto(Drivetrain drivetrain, NavX navX, IntakeArmSolenoid intakeArmSolenoid, Limelight limelight, Turret turret) {
      super(
        new InstantCommand(), // TODO: Enable rollers
        new StraightDrive(drivetrain, navX, 0.5, 90), // TODO: Change distance later
        new TurnDrivetrain(drivetrain, navX, -100),
        new StraightDrive(drivetrain, navX, 0.5, 2),
        new TurnDrivetrain(drivetrain, navX, -100),
        new InstantCommand(() -> intakeArmSolenoid.push(), intakeArmSolenoid),
        new StraightDrive(drivetrain, navX, 0.5, 25),
        new InstantCommand(() -> intakeArmSolenoid.retract(), intakeArmSolenoid),
        new TurnDrivetrain(drivetrain, navX, -100),
        new InstantCommand(), // TODO: Disable rollers
        new StraightDrive(drivetrain, navX, 0.5, 40),
        new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)),
        new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)),
        new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
        new WaitUntilCommand(limelight::hasTarget), // Only Aim and continue with shooting if we have a valid target
        new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
        new InstantCommand(() -> turret.setPosition(turret.getCenter()), turret),
        new AimTurret(turret, limelight),
        new InstantCommand(), // TODO: Spin up flywheels
        new InstantCommand() // TODO: Turn on indexer
      );
    }
}
