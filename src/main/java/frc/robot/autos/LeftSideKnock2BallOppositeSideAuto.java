package frc.robot.autos;

import com.team6479.lib.util.dynamic.Limelight;
import com.team6479.lib.util.dynamic.Limelight.CamMode;
import com.team6479.lib.util.dynamic.Limelight.LEDState;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.commands.AimTurret;
import frc.robot.commands.SpinUpFlywheels;
import frc.robot.commands.StraightDrive;
import frc.robot.commands.TurnDrivetrain;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheels;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.IntakeRollers;
import frc.robot.subsystems.NavX;
import frc.robot.subsystems.Turret;

public class LeftSideKnock2BallOppositeSideAuto extends SequentialCommandGroup {
    public LeftSideKnock2BallOppositeSideAuto(Drivetrain drivetrain, NavX navX, IntakeArmSolenoid intakeArmSolenoid, IntakeRollers intakeRollers, Limelight limelight, Turret turret, Flywheels flywheels, Indexer indexer) {
      super(
        new InstantCommand(() -> intakeArmSolenoid.push(), intakeArmSolenoid),
        new InstantCommand(() -> intakeRollers.rollersOn(), intakeRollers),
        new StraightDrive(drivetrain, navX, 0.5, 90), // TODO: Change distance later
        new InstantCommand(() -> intakeArmSolenoid.retract(), intakeArmSolenoid),
        new InstantCommand(() -> intakeRollers.rollersOff(), intakeRollers),
        new TurnDrivetrain(drivetrain, navX, -100),
        new StraightDrive(drivetrain, navX, 0.5, 40),
        new TurnDrivetrain(drivetrain, navX, -120),
        new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)),
        new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)),
        new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
        new WaitUntilCommand(limelight::hasTarget), // Only Aim and continue with shooting if we have a valid target
        new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
        new InstantCommand(() -> turret.setPosition(turret.getCenter()), turret),
        new AimTurret(turret, limelight),
        new SpinUpFlywheels(flywheels, limelight),
        new InstantCommand(() -> indexer.setTop(1.0), indexer),
        new InstantCommand(() -> indexer.setBottom(1.0), indexer),
        new WaitCommand(2.0),
        new InstantCommand(() -> indexer.setTop(0.0), indexer),
        new InstantCommand(() -> indexer.setBottom(0.0), indexer)
      );

    }
}
