package frc.robot.autos;

import com.team6479.lib.util.dynamic.Limelight;
import com.team6479.lib.util.dynamic.Limelight.CamMode;
import com.team6479.lib.util.dynamic.Limelight.LEDState;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.commands.StraightDrive;
import frc.robot.commands.TurnDrivetrain;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.IntakeArmSolenoid;
import frc.robot.subsystems.NavX;
import frc.robot.subsystems.Turret;

public class DriveShootTwoAuto extends SequentialCommandGroup {
  public DriveShootTwoAuto(Drivetrain drivetrain, NavX navX, IntakeArmSolenoid intakeArmSolenoid, Limelight limelight, Turret turret) {
    super(
      new InstantCommand(() -> intakeArmSolenoid.push(), intakeArmSolenoid),
      new InstantCommand(), // TODO: Turn on rollers
      new StraightDrive(drivetrain, navX, 0.5, 100),
      new InstantCommand(), // TODO: Turn off rollers
      new InstantCommand(() -> intakeArmSolenoid.retract(), intakeArmSolenoid),
      new TurnDrivetrain(drivetrain, navX, 180),
      new InstantCommand(() -> limelight.setLEDState(LEDState.Auto)),
      new InstantCommand(() -> limelight.setCamMode(CamMode.VisionProcessor)),
      new InstantCommand(() -> DriverStation.reportWarning("Waiting For Target...", false)),
      new WaitUntilCommand(() -> limelight.hasTarget()), // Only Aim and continue with shooting if we have a valid target
      new InstantCommand(() -> DriverStation.reportWarning("Target Found. Continuing.", false)),
      new InstantCommand(() -> turret.setPosition(turret.getCenter()), turret),
      new InstantCommand(), // TODO: Aim turret
      new InstantCommand(), // TODO: Spin up flywheels
      new InstantCommand() // TODO: Turn on indexer
    );
  }
}
