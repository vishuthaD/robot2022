// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  public final class DrivetrainConstants {
    public static final int MOTOR_LEFT_FRONT = 0;
    public static final int MOTOR_LEFT_BACK = 1;
    public static final int MOTOR_RIGHT_FRONT = 2;
    public static final int MOTOR_RIGHT_BACK = 3;
  }

  public final class IntakeConstants {
    public static final int INTAKE_ARM = 0; //change later
    public static final int INTAKE_ARM_LIMIT_SWITCH_FRONT = 0;
    public static final int INTAKE_ARM_LIMIT_SWITCH_BACK = 1;
    public static final int INTAKE_ARM_SOLENOID_FORWARD = 0;
    public static final int INTAKE_ARM_SOLENOID_REVERSE = 0;
  }

  public final class TurretConstants {
    public static final int MOTOR = 0;
  }

  public final class FlywheelConstants {
    public static final int SMALL_RIGHT = 0;
    public static final int SMALL_LEFT = 0;
    public static final int BIG_RIGHT = 0;
    public static final int BIG_LEFT = 0;
  }

  public final class ClimberConstants {
    public static final int PISTON_FORWARD = 0;
    public static final int PISTON_BACKWARD = 0;
    public static final int MOTOR = 0;
  }

  public final class IntakeRollerConstants {
    public static final int INTAKE_ROLLER = 9;
    public static final int INTAKE_ARM = 8;
    public static final int INTAKE_ARM_LIMIT_SWITCH_FRONT = 0;
    public static final int INTAKE_ARM_LIMIT_SWITCH_BACK = 1;
    public static final int INTAKE_ARM_PDP = 5;
  }

  public final class IndexerConstants {
    public static final int MOTOR_TOP = 0;
    public static final int MOTOR_BOTTOM = 0;
  }
}

