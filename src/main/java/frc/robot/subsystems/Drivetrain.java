// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.team6479.lib.subsystems.TankDrive;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.DrivetrainConstants;

public class Drivetrain extends SubsystemBase implements TankDrive {
  /** Creates a new Drivetrain. */

  private TalonFX motorLeftFront;
  private TalonFX motorLeftBack;
  private TalonFX motorRightFront;
  private TalonFX motorRightBack;

  // TODO: one wheel rotation = 6.11 motor rotations
  public Drivetrain() {

    motorLeftFront = new TalonFX(DrivetrainConstants.MOTOR_LEFT_FRONT);
    motorLeftBack = new TalonFX(DrivetrainConstants.MOTOR_LEFT_BACK);
    motorRightFront = new TalonFX(DrivetrainConstants.MOTOR_RIGHT_FRONT);
    motorRightBack = new TalonFX(DrivetrainConstants.MOTOR_RIGHT_BACK);

    // Reset to factory defaults to ensure no config carryover
    motorLeftFront.configFactoryDefault();
    motorLeftBack.configFactoryDefault();
    motorRightFront.configFactoryDefault();
    motorRightBack.configFactoryDefault();

    // Have back motors follow front motors
    motorLeftBack.follow(motorLeftFront);
    motorRightBack.follow(motorRightFront);

    // Set the neutral mode
    motorLeftFront.setNeutralMode(NeutralMode.Brake);
    motorLeftBack.setNeutralMode(NeutralMode.Brake);
    motorRightFront.setNeutralMode(NeutralMode.Brake);
    motorRightBack.setNeutralMode(NeutralMode.Brake);

    // Setup encoders
    motorLeftFront.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);
    motorRightFront.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);

    // Invert Right Motors
    motorLeftFront.setInverted(false);
    motorLeftBack.setInverted(false);
    motorRightFront.setInverted(true);
    motorRightBack.setInverted(true);


  }

  public double getPosition() {
    double leftSide = motorLeftFront.getSelectedSensorPosition();
    double rightSide = motorRightFront.getSelectedSensorPosition();

    return (leftSide + rightSide) / 2;
  }

  public void resetEncoders() {
    motorLeftFront.setSelectedSensorPosition(0, 0, 20);
    motorRightFront.setSelectedSensorPosition(0, 0, 20);
  }


  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
  @Override
  public void stop() {
    motorLeftFront.set(ControlMode.PercentOutput, 0);
    motorRightFront.set(ControlMode.PercentOutput, 0);
  }

  @Override
  public void arcadeDrive(double forward, double turn) {
    motorLeftFront.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, +turn);
    motorRightFront.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, -turn);
  }

  @Override
  public void tankDrive(double leftSpeed, double rightSpeed) {
    motorLeftFront.set(ControlMode.PercentOutput, leftSpeed);
    motorRightFront.set(ControlMode.PercentOutput, rightSpeed);
  }

}
