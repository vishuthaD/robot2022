// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.SparkMaxPIDController;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.FlywheelConstants;

public class Flywheels extends SubsystemBase {

  private CANSparkMax smallFlywheel1;
  private CANSparkMax smallFlywheel2;
  private SparkMaxPIDController smallPIDController;
  private TalonFX bigFlywheelMotor1;
  private TalonFX bigFlywheelMotor2;
  private boolean isBigOn, isSmallOn;
  private double a, b;

  private final double RPM_TOLERANCE = 50;

  public Flywheels(double a, double b) {
    smallFlywheel1 = new CANSparkMax(FlywheelConstants.SMALL_LEFT, MotorType.kBrushless);
    smallFlywheel2 = new CANSparkMax(FlywheelConstants.SMALL_RIGHT, MotorType.kBrushless);

    smallFlywheel1.restoreFactoryDefaults();
    smallFlywheel2.restoreFactoryDefaults();

    bigFlywheelMotor1 = new TalonFX(FlywheelConstants.BIG_LEFT);
    bigFlywheelMotor2 = new TalonFX(FlywheelConstants.BIG_RIGHT);

    bigFlywheelMotor1.configFactoryDefault();
    bigFlywheelMotor2.configFactoryDefault();

    smallFlywheel2.setInverted(true);

    bigFlywheelMotor1.setInverted(false);
    bigFlywheelMotor2.setInverted(true);

    smallFlywheel1.follow(smallFlywheel2, true);
    bigFlywheelMotor2.follow(bigFlywheelMotor1);

    bigFlywheelMotor1.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor, 0, 30); // source: ctre examples

    // PIDF tuning constants
    bigFlywheelMotor1.config_kF(0, .055);
		bigFlywheelMotor1.config_kP(0, 0.15);
		bigFlywheelMotor1.config_kI(0, 0);
		bigFlywheelMotor1.config_kD(0, 0);

    // REV uses a seperate PID controller object
    smallPIDController = smallFlywheel2.getPIDController();

    smallPIDController.setP(0.0001);
    smallPIDController.setI(0);
    smallPIDController.setD(0);
    smallPIDController.setIZone(100);
    smallPIDController.setFF(.000095);

    isBigOn = false;
    isSmallOn = false;

    this.a = a;
    this.b = b;
  }

  public static double bigRPMToNative(double rpm) {
    double BIG_CPR = 4096; // encoder units per rotation
    double BIG_UNITS_PER_MIN = rpm * BIG_CPR; // Convert to encoder units per minute
    double BIG_UNITS_PER_TENTH = BIG_UNITS_PER_MIN / 600; // 600 deciseconds in a minute
    return BIG_UNITS_PER_TENTH;
  }

  public void setSmallFlywheelRawSpeed(double speed) {
    smallFlywheel1.set(speed);
    smallFlywheel2.set(speed);
    isSmallOn = (speed != 0);
  }

  public void setBigFlywheelRawSpeed(double speed) {
    bigFlywheelMotor1.set(ControlMode.PercentOutput, speed);
    isBigOn = (speed != 0);
  }

  /**
   * Uses regression equation from real world testing to determine the top wheel rpm, along with distance to the target calculated with the limelight
   * @param distance Distance to the target
   */
  public double getTopRPM(double distance) {
    return (a * distance) + b;
  }

  /**
   * Sets the PIDF setpoint of the small flywheel
   * @param speed The rotational velocity in RPM
   */
  public void setSmallFlywheelSpeed(double speed) {
    // REV uses RPM natively
    smallPIDController.setReference(speed, CANSparkMax.ControlType.kVelocity);
    isSmallOn = (speed != 0);
  }

  /**
   * Sets the PIDF setpoint of the large flywheel
   * @param speed The rotational velocity in RPM
   */
  public void setBigFlywheelSpeed(double speed) {
    // CTRE uses cycles/decisecond natively, hence the conversion
    bigFlywheelMotor1.set(ControlMode.Velocity, bigRPMToNative(speed));
    isBigOn = (speed != 0);
  }

  public void off() {
    setBigFlywheelRawSpeed(0);
    isBigOn = false;
    setSmallFlywheelRawSpeed(0);
    isSmallOn = false;
  }

  public void setRawSpeed(double big, double small) {
    setBigFlywheelRawSpeed(big);
    setSmallFlywheelRawSpeed(small);
  }

  /**
   * Sets the PIDF setpoint of both flywheels
   * @param big The rotational velocity of the large flywheel in RPM
   * @param small The rotational velocity of the small flywheel in RPM
   */
  public void setSpeed(double big, double small) {
    setBigFlywheelSpeed(big);
    setSmallFlywheelSpeed(small);
  }

  /**
   * @param distance Distance from the target
   * @return true if the RPM of the top wheel is within the tolerance
   */
  public boolean isWithinTolerance(double distance) {
    return Math.abs(getSmallSpeed() - getTopRPM(distance)) < RPM_TOLERANCE;
  }

  public boolean getIsOn() {
    return isBigOn || isSmallOn;
  }

  public double getSmallSpeed() {
    return smallFlywheel1.getEncoder().getVelocity();
  }

}
