// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Constants.IntakeConstants;

public class IntakeArm extends SubsystemBase {

  public enum Position {
    In(600), Out(-200);

    public double value;

    private Position(double value) {
      this.value = value;
    }
  }

  /** Creates a new IntakeArm. */

  private Position position = Position.In; //Status of the arm

  private final TalonSRX intakeArm = new TalonSRX(Constants.IntakeConstants.INTAKE_ARM);

  private final DigitalInput limitSwitchFront =
  new DigitalInput(IntakeConstants.INTAKE_ARM_LIMIT_SWITCH_FRONT);
  private final DigitalInput limitSwitchBack =
  new DigitalInput(IntakeConstants.INTAKE_ARM_LIMIT_SWITCH_BACK);



  public IntakeArm() {

    intakeArm.configFactoryDefault();
    intakeArm.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);
    intakeArm.setInverted(false);

  }

  public boolean isOut() {
    return position == Position.Out;
  }

  public void armStop() {
    intakeArm.set(ControlMode.PercentOutput, 0.0);
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  public void set(double speed) {
    intakeArm.set(ControlMode.PercentOutput, speed);
  }

  public double getPosition() {
    return intakeArm.getSelectedSensorPosition();
  }

  public double getCurrent() {
    return intakeArm.getStatorCurrent();
  }

  public boolean isAtFrontLimit() {
    return !limitSwitchFront.get();
  }

  public boolean isAtBackLimit() {
    return !limitSwitchBack.get();
  }

}
